# DataINRAE-API-Examples

[![License](https://img.shields.io/badge/license-MIT-green)](https://opensource.org/licenses/MIT)

Run notebooks on binder : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fforgemia.inra.fr%2Fdimitri.szabo%2Fdatainrae-api-examples/master)
